### Lab fix

```
FROM alpine:3.7
MAINTAINER Glenn ten Cate <glenn.ten.cate@owasp.org>
RUN apk update --no-cache && apk add python3 \
python3-dev \
py3-pip \ 
git \
bash \
imagemagick

RUN addgroup -S app && adduser -S -G app app 
WORKDIR /skf-labs/CMD
COPY . .
RUN chmod 700 secret-file
RUN pip3 install -r requirements.txt

USER app
CMD [ "python3", "./CMD.py" ]
```

After building this new image, try your trick again!