# Challenge 5

This challenge describes how to run our SAST (Bandit) scan

----


### Pull the Bandit container

##### command:

``` bash
docker pull secfigo/bandit
```

#### output:

```bash
Using default tag: latest
latest: Pulling from secfigo/bandit
ff3a5c916c92: Pull complete 
dd7b62403fcc: Downloading [=====================>                             ]  6.956MB/16.26MB
5a9f804f5068: Download complete 
......... snip ........
```

----

### Change your directory to the flask-app dir

##### command:

```bash
cd flask-app
```
--

### Start our bandit scanner

##### command:

Note that $(pwd) is only supported on Linux and MacOS - on Windows you will need to replace this with the full current working directory.

```bash
docker run --rm --volume $(pwd):/src secfigo/bandit
```

##### output:

```bash
Issue Fingerprint: 13454714fb2086fa556860242503642a1628283f83c742d1a5e52ff4106e0566
Issue Severity: MEDIUM 	 Confidence Level: HIGH
Location: /src/DES.py
Issue: Use of unsafe yaml load. Allows instantiation of arbitrary objects. Consider yaml.safe_load().

Code: 
20             yaml_file = base64.b64decode(input)
21             content = yaml.load(yaml_file)
22     except:

--------------------------------------------------
Issue Fingerprint: f7780cc7b1ca3d888ae7718c1c0c4dc3be55ef4ccec99bd33d455ed3e9c02506
Issue Severity: MEDIUM 	 Confidence Level: MEDIUM
Location: /src/DES.py
Issue: Possible binding to all interfaces.

Code: 
31 if __name__ == "__main__":
32     app.run(host='0.0.0.0')

--------------------------------------------------
Issue Fingerprint: afa257084f161191194de4a5fa7ae9a97f79b7aa8ae0f5a538c968fcfc62ddfa
Issue Severity: MEDIUM 	 Confidence Level: MEDIUM
Location: /src/evil_server.py
Issue: Possible binding to all interfaces.

Code: 
11 if __name__ == "__main__":
12     app.run(host='0.0.0.0', port=1337)
13 	

Code scanned:
          Total lines of code: 33
          Total false positives: 2

Total issues (by severity):
          High: 0
          Medium: 3
          Low: 0

Total issues (by confidence):
          High: 1
          Medium: 2
          Low: 0
```
----